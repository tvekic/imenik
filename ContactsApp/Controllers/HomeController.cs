﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ContactsApp.Models;

namespace ContactsApp.Controllers
{
    public class HomeController : Controller
    {
        ContactsDBContext _db = new ContactsDBContext();
        public ActionResult Index()
        {

            return View();
        }


           [HttpGet]
        public ActionResult GetContacts()
        { 
            var contacts = (from c in _db.Contacts
                            join e in _db.Emails on c.Id equals e.ContactId into g
                            join pn in _db.Numbers on c.Id equals pn.ContactId into pnc
                            join tg in _db.Tags on c.Id equals tg.ContactId into tgc
                            select new { 
                                c.Name,
                                c.Surname,
                                c.Address,
                                Emails = g.Select(x => x.TheEmail),
                                Numbers = pnc.Select(y => y.PhoNumb),
                                Tags = tgc.Select(z => z.TheTag )
                            }
                                );
            return Json(contacts, JsonRequestBehavior.AllowGet);
        }

      


        
    }
}
