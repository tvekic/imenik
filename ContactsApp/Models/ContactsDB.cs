﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ContactsApp.Models
{
    public class Contact
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public ICollection<Email> Emails { get; set; }
        public ICollection<Number> Numbers { get; set; }
        public ICollection<Tag> Tags { get; set; }

    }
    public class Email
    {
        public int Id { get; set; }
        public string TheEmail { get; set; }
        public int ContactId { get; set; }
    }

    public class Number
    {
        public int Id { get; set; }
        public string PhoNumb { get; set; }
        public int ContactId { get; set; }

    }

    public class Tag
    {
        public int Id { get; set; }
        public string TheTag { get; set; }
        public int ContactId { get; set; }

    }

    public class ContactsDBContext : DbContext
    {
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<Number> Numbers { get; set; }
        public DbSet<Tag> Tags { get; set; }
    }
}